<table class="table">
    <tr>
        <th>#</th>
        <th>a</th>
        <th>b</th>
        <th>c</th>
        <th>d</th>
        <th>e</th>
        <th>f</th>
        <th>g</th>
        <th>h</th>
        <th>i</th>
        <th>j</th>
    </tr>
    @foreach($grid as $key => $row)
        <tr>
            <td><b>{{ $key }}</b></td>
            @foreach($row as $cell)

                <td class="@if($cell < 0 ) table-success @elseif($cell === 0) table-danger @endif">@if( $show ) {{ $cell }} @endif</td>

            @endforeach
        </tr>
    @endforeach
</table>
