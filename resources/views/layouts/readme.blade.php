<div class="card mt-3">
    <div class="card-header">
        Readme:
    </div>
    <div class="card-body">
        {{--@if($game['type'] == 'multi')--}}
            <p>
                By default, ships are hidden, otherwise we spoil the game ;). But when you press the button "show" ships,
                all ships will be revealed.
            </p>
        {{--@endif--}}
        <p>The numbers on the grid match the numbers of the list on the scoreboard. A negative value means you hit something,
            a 0 you missed.
        </p>
    </div>
</div>