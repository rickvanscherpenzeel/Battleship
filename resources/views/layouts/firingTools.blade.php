<div class="card">
    <div class="card-body">
        <p>
            Enter Coordinates and press fire to attack a cell
        </p>
        <form method="POST" action="/games/{{$game->id}}@if ($game['type'] == 'multi')/{{$playerId}}@endif"
              class="form-inline">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="coordinate_x">X: </label>
                <input type="text" class="form-control ml-1" id="coordinate_x" name="coordinate_x"
                       required>
            </div>
            <div class="form-group ml-5">
                <label for="coordinate_x">Y: </label>
                <input type="text" class="form-control ml-1" id="coordinate_y" name="coordinate_y"
                       required>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary ml-2">Fire</button>
            </div>
            @include('layouts.errors')
        </form>

        {{--@if($game['type'] == 'multi')--}}
            <div class="form-group mt-3">
                <a href="/games/{{$game->id}}@if ($game['type'] == 'multi')/{{$playerId}} @endif"
                   class="btn btn-primary" role="button">Hide ships</a>
                <a href="/games/{{$game->id}}@if ($game['type'] == 'multi')/{{$playerId}}@endif?show=true"
                   class="btn btn-primary" role="button">Show ships</a>
            </div>
        {{--@endif--}}

    </div>
</div>