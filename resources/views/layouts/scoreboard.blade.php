<ul class="list-group">
    <li class="list-group-item list-group-item-dark">Score</li>
    @foreach($ships as $key => $ship)
        <li class="list-group-item @if ($ship['sunk']) list-group-item-success @endif">
            #{{$key}}: {{ $ship['type'] }} @if(!$scoreHidden) | hits: {{ $ship['hits'] }}/{{ ($ship['length']*$ship['width']) }} @endif
        </li>
    @endforeach
</ul>
