@extends('layout')

@section('content')
    <div class="offset-sm-3 col-sm-6 text-center">
        @foreach (['danger', 'warning', 'success', 'info'] as $key)
            @if(Session::has($key))
                <p class="alert alert-{{ $key }}">{{ Session::get($key) }}</p>
            @endif
        @endforeach
        <div class="card">
            <div class="card-body">
                <h3>Welcome at BattleShip</h3>
                <hr>
                    <h5>SinglePlayer Game</h5>
                    <div class="form-group">
                        <a href="/games/create/single" class="btn btn-primary" role="button">Start SinglePlayer Game</a>
                    </div>

                <ul class="list-group">
                    @foreach($singleGames as $game)
                        <li class="list-group-item @if($game['isActive'] == 0)list-group-item-danger @endif">
                            #{{ $game->id }}:
                            <a href="/games/{{$game->id}}/1" class="btn btn-primary" role="button">Continue game</a>
                        </li>
                    @endforeach
                </ul>

                <hr>
                <h5>MultiPlayer Game</h5>
                <div class="form-group">
                    <a href="/games/create/multi" class="btn btn-primary" role="button">Create MultiPlayer Game</a>
                </div>

                <ul class="list-group">
                    @foreach($multiGames as $game)
                        <li class="list-group-item @if($game['isActive'] == 0)list-group-item-danger @endif">
                            #{{ $game->id }}:
                            <a href="/games/{{$game->id}}/1" class="btn btn-primary" role="button">Join as Player1</a>
                            <a href="/games/{{$game->id}}/2" class="btn btn-primary" role="button">Join as Player2</a>
                        </li>
                    @endforeach
                </ul>

            </div>
        </div>
    </div>
@endsection