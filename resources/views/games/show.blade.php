@extends('layout')

@section('content')
    <div class="row">
        <div class="col-sm-8">
            @foreach (['danger', 'warning', 'success', 'info'] as $key)
                @if(Session::has($key))
                    <p class="alert alert-{{ $key }}">{{ Session::get($key) }}</p>
                @endif
            @endforeach
            <h5>Opponents Board @if($game['type'] == 'multi')(Your board is Below) @endif</h5>
            @include('layouts.grid', ['grid' => $grid1])


            @include('layouts.firingTools')

        </div>
        <div class="col-sm-4">
            <h5>Your Score</h5>
            @include('layouts.scoreboard', ['ships' => $ships1, 'scoreHidden' => true])

            @include('layouts.readme')
        </div>
    </div>

    @if($game['type'] == 'multi')
        <div class="row mt-5">
            <div class="col-sm-8">
                <h5>Your Board</h5>
                @include('layouts.grid', ['grid' => $grid2, 'show' => true])
            </div>
            <div class="col-sm-4">
                <h5>Opponents Score</h5>
                @include('layouts.scoreboard', ['ships' => $ships2, 'scoreHidden' => false])
            </div>
        </div>
    @endif


@endsection
