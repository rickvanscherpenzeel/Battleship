<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $casts = [
        'player1_grid' => 'array',
        'player1_ships' => 'array',
        'player2_grid' => 'array',
        'player2_ships' => 'array',
    ];

//    public function getPlayer1GridAttribute($value)
//    {
//        return json_decode($value, true);
//    }
//
//    public function getPlayer1ShipsAttribute($value)
//    {
//        return json_decode($value, true);
//    }
//
//    public function getPlayer2GridAttribute($value)
//    {
//        return json_decode($value, true);
//    }
//
//    public function getPlayer2ShipsAttribute($value)
//    {
//        return json_decode($value, true);
//    }

}
