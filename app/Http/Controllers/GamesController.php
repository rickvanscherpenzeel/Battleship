<?php

namespace App\Http\Controllers;

use App\Game;
use App\Board;
use Carbon\Carbon;

class GamesController extends Controller
{
    public function index()
    {
        $multiGames = Game::Where('type', 'multi')->where('isActive', 1)->get();
        $singleGames = Game::where('type', 'single')->where('isActive', 1)->get();
        return view('games.index', compact('multiGames', 'singleGames'));

    }

    public function createSingle()
    {
        $game = new Game();
        $board = Board::getNewBoard();

        $game->type = 'single';
        $game->player1_grid = $board->getGrid();
        $game->player1_ships = $board->getShips();

        $game->save();

        return redirect('/games/' . $game->id);


    }

    public function createMulti()
    {
        $game = new Game();
        $board = Board::getNewBoard();

        $game->type = 'multi';
        $game->player1_grid = $board->getGrid();
        $game->player1_ships = $board->getShips();

        $board = Board::getNewBoard();
        $game->player2_grid = $board->getGrid();
        $game->player2_ships = $board->getShips();

        $game->save();

        return redirect('/');


    }

    public function show(Game $game, $playerId = 1)
    {
        $show = request()->input('show');

        if ($playerId == 1) {

            $grid1 = $game->player1_grid;
            $ships1 = $game->player1_ships;
            $grid2 = $game->player2_grid;
            $ships2 = $game->player2_ships;

        } else {
            $grid1 = $game->player2_grid;
            $ships1 = $game->player2_ships;
            $grid2 = $game->player1_grid;
            $ships2 = $game->player1_ships;
        }

        return view('games.show', compact('game', 'show', 'playerId', 'grid1', 'grid2', 'ships1', 'ships2'));

    }

    public function update(Game $game, $playerId = 1)
    {
        $this->validate(request(), [
            'coordinate_x' => 'required|alpha|regex:/^[a-j]+$/u',
            'coordinate_y' => 'required|numeric|not_in:0'
        ]);

        $board = Board::getBoardFromData($game, $playerId);


        $shipIdHit = $board->fireAtShip(request()->all());

        if ($this->isGameFinished($game, $board, $playerId)) {
            return redirect('/');
        }

        if ($shipIdHit) {

            $ship = $board->getShips()[$shipIdHit];

            //if hits == length of ship, the ship has sunk, so we should tell the player
            if ($ship['hits'] == ($ship['length'] * $ship['width'])) {
                session()->flash('success', 'You SUNK a ' . $ship['type'] . ", Nice job");
            } else {
                session()->flash('success', 'Hit!');
            }

        } else {

            session()->flash('danger', 'No hit, keep trying');
        }

        $this->saveGrid($game, $board, $playerId);

        //redirect back to game.
        return redirect('/games/' . $game->id . '/' . $playerId);
    }

    private function isGameFinished($game, $board, $playerId)
    {

        if (!$game['isActive']) {
            session()->flash('success', 'The game has finished');
            return true;
        }

        //if all ships are sunk, we can stop the game and go to homepage.
        if ($board->allShipsSunk()) {
            $game->isActive = false;
            $game->winner_user_id = $playerId;
            $game->finished_at = Carbon::now()->format('Y-m-d H:i:s');
            $game->save();

            session()->flash('success', 'You won the game, Congratulations');

            $this->saveGrid($game, $board, $playerId);

            return true;
        }

        return false;

    }

    private function saveGrid($game, $board, $playerId)
    {
        //save new grid and ships to game
        if ($playerId == 1) {

            $game->player1_grid = $board->getGrid();
            $game->player1_ships = $board->getShips();

        } else {
            $game->player2_grid = $board->getGrid();
            $game->player2_ships = $board->getShips();
        }

        $game->save();
    }
}
