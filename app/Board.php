<?php


namespace App;

class Board
{

    public $grid = null;
    public $ships = null;

    //load board with data from DB
    public static function getBoardFromData($game, $playerId)
    {
        $board = new self;

        if ($playerId == 1) {
            //use grid data from db when re-instantiating the class.
            $board->setGrid($game->player1_grid);
            //use ships data from db when re-instantiating the class.
            $board->setShips($game->player1_ships);

        } else {
            //use grid data from db when re-instantiating the class.
            $board->setGrid($game->player2_grid);
            //use ships data from db when re-instantiating the class.
            $board->setShips($game->player2_ships);
        }

        return $board;
    }

    //Create new board instance
    public static function getNewBoard()
    {
        $board = new self;
        $board->createBoardGrid();
        $board->populateNewBoard();

        return $board;
    }

    public function getGrid()
    {
        return $this->grid;
    }

    public function getShips()
    {
        return $this->ships;
    }

    public function allShipsSunk()
    {

        foreach ($this->ships as $ship) {

            if (!$ship['sunk']) {
                return false;
            }
        }

        return true;

    }

    public function fireAtShip($coordinates)
    {
        //convert a-j coordinates to numbers
        $x = $this->toNumber($coordinates['coordinate_x']);
        $y = $coordinates['coordinate_y'];

        //check if cell is empty, or allready shot at, then we miss, so return false
        if (is_null($this->grid[$y][$x]) || $this->grid[$y][$x] <= 0) {

            //When we fire and miss, we set the cell value to 0
            if (is_null($this->grid[$y][$x])) {
                $this->grid[$y][$x] = 0;
            }
            return false;
        }

        //we hit the ship, so we increment hits
        $this->ships[$this->grid[$y][$x]]['hits']++;

        //if hits == length of ships, the ship is sunk
        if ($this->ships[$this->grid[$y][$x]]['hits'] == ($this->ships[$this->grid[$y][$x]]['length'] * $this->ships[$this->grid[$y][$x]]['width'])) {
            $this->ships[$this->grid[$y][$x]]['sunk'] = true;
        }

        $shipId = $this->grid[$y][$x];

        //When we fire and hit, we set the cell value to the negative of the shipId, so we can still match it to a specific ship
        $this->grid[$y][$x] = -$shipId;

        //return ID of ship we hit, to tell player what he hit
        return $shipId;

    }

    public function getDefaultShips()
    {

        //we need to start the index at 1, otherwise we get zeros on the board (array key will be uses as reference to
        // the board) and that will give us trouble later. So this is easy fix.
        $index = 0;

        foreach (Ship::orderBy('length', 'desc')->get() as $ship) {

            foreach (range(1, $ship['number_in_fleet']) as $number) {
                $ship['hits'] = 0;
                $ship['sunk'] = false;
                $index++;
                $ships[$index] = $ship;

            }
        }

        return $ships;
    }

    private function populateNewBoard()
    {
        $ships = $this->getDefaultShips();

        //fill a empty grid with ships. this function tries to add a ships.
        foreach ($ships as $shipId => $shipData) {

            do {
                $added = $this->addShipToGrid($shipId, $shipData, $this->grid);

            } while ($added == false);
        }

        //add list of ships in game to board.
        $this->ships = $ships;
    }

    private function addShipToGrid($shipId, $shipData, $grid)
    {

        // get random cell and direction to place ship on grid
        $direction = rand(0, 1) == 0 ? 'hor' : 'ver';

        $coordinates = [
            'x' => rand(1, 10),
            'y' => rand(1, 10),
        ];

        //check if ship will fit
        $maxLength = $direction == 'hor' ? ($coordinates['x'] + $shipData['length'] - 1) : ($coordinates['y'] + $shipData['length'] - 1);

        if ($maxLength <= 10) {

            //based on the ships length we loop x cells into the right direction and place ship on those cells
            foreach (range(0, $shipData['width'] - 1) as $offset) {

                if ($direction == 'hor') {
                    $coordinates['y'] = $coordinates['y'] + $offset;
                } else {
                    $coordinates['x'] = $coordinates['x'] + $offset;
                }

                foreach (range(0, $shipData['length'] - 1) as $offset) {

                    //get new coordinates to add ship to cell
                    $nextCoordinates = $this->getNewCoordinates($coordinates, $direction, $offset);

                    //check if cell is empty, if not, ships will cant be placed here, so try again on other location
                    if (is_null($grid[$nextCoordinates['y']][$nextCoordinates['x']])) {

                        $grid[$nextCoordinates['y']][$nextCoordinates['x']] = $shipId;

                    } else {

                        return false;

                    }
                }
            }
            //all good, so we update the grid with the new ship.
            $this->grid = $grid;
            return true;
        }

    }

    private function getNewCoordinates($coordinates, $direction, $offset)
    {

        //for ships placed horizontal we only look RIGHT when placing the ship on the grid.
        if ($direction == 'hor') {
            $newCoordinates = [
                'x' => $coordinates['x'] + $offset,
                'y' => $coordinates['y'],
            ];
        } else {
            //for ships placed vertical we only look DOWN when placing the ship on the grid.
            $newCoordinates = [
                'x' => $coordinates['x'],
                'y' => $coordinates['y'] + $offset,
            ];

        }

        return $newCoordinates;
    }


    //create empty grid for new game board
    private function createBoardGrid()
    {

        //(y, x)
        $this->grid = [];

        foreach(range(1, 10) as $y){

            $this->grid[$y] = [];

            foreach(range(1, 10) as $x){
                $this->grid[$y][$x] = null;
            }
        }

    }

    private function setGrid($boardData)
    {
        $this->grid = $boardData;
    }

    private function setShips($ships)
    {
        $this->ships = $ships;
    }

    //transform letter coordinate to number (a-j -> 1-10)
    function toNumber($letter)
    {
        return array_search($letter, range('a', 'z')) + 1;
    }
}