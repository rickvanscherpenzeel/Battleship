# BattleShip
Simple version of BattleShip. You can have a single Player or Two Player Game.

The application also support non-standard ships, but has no interface for this, they need ot be added to DB table 'ships' if wanted.

Two Player mode has an interface for each player.

##Requirements:
* Laravel 5.6
* MySql 5.7

##Install:

* Run Laravel Project on local Server
* Create Database "battle_ship"
* Run "php artisan migrate --seed"
