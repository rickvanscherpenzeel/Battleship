<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ShipsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ships')->insert([
            [
                'type' => "Aircraft carrier",
                'width' => 1,
                "length" => 5,
                "number_in_fleet" => 1,
                "created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'type' => "Battleship",
                'width' => 1,
                "length" => 4,
                "number_in_fleet" => 2,
                "created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'type' => "Submarine",
                'width' => 1,
                "length" => 3,
                "number_in_fleet" => 3,
                "created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'type' => "Patrol vessel",
                'width' => 1,
                "length" => 2,
                "num_in_fleet" => 4,
                "created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s'),
            ],

        ]);
    }
}
