<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'GamesController@index');
Route::get('/games/create/single', 'GamesController@createSingle');
Route::get('/games/create/multi', 'GamesController@createMulti');
Route::get('/games/{game}/{player?}', 'GamesController@show');

Route::post('/games/{game}/{player?}', 'GamesController@update');
Route::post('games', 'GamesController@store');


